import React, { useState } from 'react';
import style from './styles/StoryInput.module.scss';

export default function StoryInput() {

  const [userStory, setUserStory] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    if(userStory.length > 50) {
      console.log(userStory, 'submitted');
    } else {
      console.log('not enough place');
    }
  }

  return (
    <form className={style.wrap}>
      <div className={style.text_area_wrap}>
        <div className={style.text_area_box}>
          <textarea 
          className={style.text_area} 
          name="myInput" 
          placeholder="Tell us about your story..." 
          cols="100" 
          rows="10" 
          minLength="50" 
          maxLength="500" 
          required
          onChange={(e) => {
            setUserStory(e.target.value);
          }}
          >
          </textarea>
        </div>
      </div>
      <button 
      className={style.submit_button} 
      onClick={(e) => {handleSubmit(e)}}
      type="submit" 
      name="submitUserStory"
      >
        Share my story
      </button>
    </form>
  )
}

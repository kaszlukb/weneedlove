import React from 'react';
import OneStoryCard from './OneStoryCard';

export default function MainLayout() {

  const fakeStory = {
    title: 'My problem',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pharetra mi eu rutrum aliquet. Donec non suscipit turpis. Maecenas elit justo, rutrum et metus non, ullamcorper volutpat mauris. Cras nec rhoncus mi. Vivamus vitae nunc purus. Aliquam suscipit, odio nec. ',
    image: 'https://cdn.pixabay.com/photo/2021/02/15/08/31/woman-6017024_960_720.jpg'
  };

  return (
    <div>
      <OneStoryCard story={fakeStory} />
    </div>
  )
}
